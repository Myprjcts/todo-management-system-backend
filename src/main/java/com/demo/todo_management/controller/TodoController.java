package com.demo.todo_management.controller;

import com.demo.todo_management.domain.Todo;
import com.demo.todo_management.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mohammad Abusbeih
 */

@RestController
@RequestMapping(value = "/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @PostMapping
    public Todo createTodo(@RequestBody Todo todo) {
        return todoService.create(todo);
    }

//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/list")
    public Page<Todo> findAll(Pageable pageable) {
        return todoService.findAll(pageable);
    }

    @PutMapping
    public Todo updateTodo(@RequestBody Todo todo) {
        return todoService.update(todo);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteTodo(@PathVariable Long id) {
        todoService.delete(id);
    }
}
