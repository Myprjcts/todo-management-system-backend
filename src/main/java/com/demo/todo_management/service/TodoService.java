package com.demo.todo_management.service;

import com.demo.todo_management.domain.Todo;

/**
 * @author Mohammad Abusbeih
 */
public interface TodoService extends CrudService<Todo> {
}
