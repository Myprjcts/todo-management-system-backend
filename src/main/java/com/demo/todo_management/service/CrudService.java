package com.demo.todo_management.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Mohammad Abusbeih
 */
public interface CrudService<T> {
    T create(T t);

    T findById(Long id);

    Page<T> findAll(Pageable pageable);

    T update(T t);

    void delete(Long id);
}
