package com.demo.todo_management.service;

import com.demo.todo_management.domain.User;

/**
 * @author Mohammad Abusbeih
 */
public interface UserService extends CrudService<User> {
}
