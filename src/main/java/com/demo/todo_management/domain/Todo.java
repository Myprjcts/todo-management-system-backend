package com.demo.todo_management.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Mohammad Abusbeih
 */

@Entity
@Table(name = "todo")
public class Todo implements Serializable {

    @Id
    @Column(name = "todo_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long todoId;

    @Column(name = "todo_title")
    private String todoTitle;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @Column(name = "user_id")
    private Long userId;

    /**
     * @return value of todoId
     */
    public Long getTodoId() {
        return todoId;
    }

    /**
     * @param todoId
     */
    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    /**
     * @return value of todoTitle
     */
    public String getTodoTitle() {
        return todoTitle;
    }

    /**
     * @param todoTitle
     */
    public void setTodoTitle(String todoTitle) {
        this.todoTitle = todoTitle;
    }

    /**
     * @return value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return value of userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
