package com.demo.todo_management.repository;

import com.demo.todo_management.domain.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Mohammad Abusbeih
 */
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
